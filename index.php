<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  $messages = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты отправлены.';
  }
  if (!empty($_COOKIE['notsave'])) {
    setcookie('notsave', '', 100000);
    $messages[] = 'Ошибка отправления.';
  }

  $errors = array();
  $errors['name'] = empty($_COOKIE['name_error']) ? '' : $_COOKIE['name_error'];
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['powers'] = !empty($_COOKIE['powers_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);

  if ($errors['name'] == 'null') {
    setcookie('name_error', '', 100000);
    $messages[] = '<div>Заполните имя.</div>';
  }
  else if ($errors['name'] == 'incorrect') {
      setcookie('name_error', '', 100000);
      $messages[] = '<div>Недопустимые символы. Введите имя заново.</div>';
  }
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div>Заполните почту.</div>';
  }
  if ($errors['powers']) {
    setcookie('powers_error', '', 100000);
    $messages[] = '<div>Выберите хотя бы одну сверхспособность.</div>';
  }
  if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages[] = '<div>Напишите что-нибудь о себе.</div>';
  }
  if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $messages[] = '<div>Вы не можете отправить форму не согласившись с контрактом.</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $powers = array();
  $powers['levit'] = "Левитация";
  $powers['tp'] = "Телепортация";
  $powers['walk'] = "Хождение сквозь стены";
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['age'] = empty($_COOKIE['age_value']) ? '' : $_COOKIE['age_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? 'male' : $_COOKIE['sex_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '4' : $_COOKIE['limbs_value'];
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  // pizdec
  if (!empty($_COOKIE['powers_value'])) {
      $powers_value = json_decode($_COOKIE['powers_value']);
  }
  $values['powers'] = [];
  if (isset($powers_value) && is_array($powers_value)) {
      foreach ($powers_value as $power) {
          if (!empty($powers[$power])) {
              $values['powers'][$power] = $power;
          }
      }
  }
  include('form.php');
}

else {
  $errors = FALSE;
  if (empty($_POST['name'])) {
    setcookie('name_error', 'null', time() + 7 * 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match("#^[aA-zZ0-9-]+$#", $_POST["name"])) {
      setcookie('name_error', 'incorrect', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
    setcookie('name_value', $_POST['name'], time() + 14 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 7 * 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 14 * 24 * 60 * 60);
  }
  $powers = array();
  foreach ($_POST['powers'] as $key => $value) {
      $powers[$key] = $value;
  }
  if (!sizeof($powers)) {
    setcookie('powers_error', '1', time() + 7 * 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('powers_value', json_encode($powers), time() + 14 * 24 * 60 * 60);
  }
  if (empty($_POST['bio'])) {
    setcookie('bio_error', '1', time() + 7 * 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bio_value', $_POST['bio'], time() + 14 * 24 * 60 * 60);
  }

  if (empty($_POST['check'])) {
    setcookie('check_error', '1', time() + 7 * 24 * 60 * 60);
    $errors = TRUE;
  }

  setcookie('age_value', $_POST['age'], time() + 14 * 24 * 60 * 60);
  setcookie('sex_value', $_POST['sex'], time() + 14 * 24 * 60 * 60);
  setcookie('limbs_value', $_POST['limbs'], time() + 14 * 24 * 60 * 60);

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('powers_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('check_error', '', 100000);
  }

    $db_user = "u16432"; // Логин БД
    $db_password = "2294342"; // Пароль БД
    $db_table = "wb3"; // Имя Таблицы БД

    $name = $_POST['name'];
    $email = $_POST['email'];
    $age = $_POST['age'];
    $sex = $_POST['sex'];
    $limbs = $_POST['limbs'];
    $bio = $_POST['bio'];
    $check = $_POST['check'];
    $powers_bd = array();
    foreach ($_POST['powers'] as $key => $value) {
        $powers_bd[$key] = $value;
    }
    $powers_string = implode(', ', $powers_bd);
    $db = new PDO('mysql:host=localhost;dbname=u16432', $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO wb3 SET name = ?, email = ?, age = ?, sex = ?, limbs = ?, powers = ?, bio = ?");
  $stmt -> execute(array(
    $_POST['name'],
    $_POST['email'],
    $_POST['age'],
    $_POST['sex'],
    $_POST['limbs'],
    implode(', ', $_POST['powers']),
    $_POST['bio']
  ));
        setcookie('save', '1');
    } catch (PDOException $e) {
        setcookie('notsave', '1');
    }
  header('Location: index.php');
}